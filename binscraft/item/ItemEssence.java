package binscraft.item;

import binscraft.BinsCraft;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemEssence extends Item {
	public ItemEssence(int id) {
		super(id);
		setMaxStackSize(64);
		setCreativeTab(CreativeTabs.tabMaterials);
		setIconIndex(7);
		setItemName("BinsCraftItemEssence");
		LanguageRegistry.addName(this, "Experience Essence");
	}
	public String getTextureFile() {
		return BinsCraft.resourceItemIconPath;
	}
}