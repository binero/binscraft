package binscraft.item;

import binscraft.BinsCraft;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemWaste extends Item {
	public ItemWaste(int id) {
		super(id);
		setMaxStackSize(64);
		setCreativeTab(CreativeTabs.tabMaterials);
		setIconIndex(8);
		setItemName("BinsCraftItemWaste");
		LanguageRegistry.addName(this, "Waste");
	}
	public String getTextureFile() {
		return BinsCraft.resourceItemIconPath;
	}
}