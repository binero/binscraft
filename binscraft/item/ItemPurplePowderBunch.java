package binscraft.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import binscraft.BinsCraft;
import binscraft.IExperienceConvertable;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class ItemPurplePowderBunch extends Item implements IExperienceConvertable {
	public int experienceValue = 18;
	public ItemPurplePowderBunch(int id) {
		super(id);
		this.setMaxStackSize(64).
		setCreativeTab(CreativeTabs.tabMaterials).
		setIconIndex(2).
		setItemName("BinsCraftItemPurplePowderBunch");
		LanguageRegistry.addName(this, "Lots of Purple Powder");
	}
	public String getTextureFile () {
		return BinsCraft.resourceItemIconPath;
	}
	
	@Override
	public int takeCharge(ItemStack itemStack, int max) {
		if(max >= experienceValue) {
			itemStack.stackSize--;
			return experienceValue;
		}
		return 0;
	}
	@Override
	public int takeCharge(ItemStack itemStack) {
		itemStack.stackSize--;
		return experienceValue;
	}
}
