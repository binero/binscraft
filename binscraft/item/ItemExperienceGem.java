package binscraft.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import binscraft.BinsCraft;
import binscraft.IExperienceChargable;
import binscraft.IExperienceContainable;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class ItemExperienceGem extends Item implements IExperienceChargable, IExperienceContainable {
	public ItemExperienceGem(int id) {
		super(id);
		setMaxStackSize(1).
		setMaxDamage(51);
		setCreativeTab(CreativeTabs.tabMisc).
		setIconIndex(3).
		setItemName("BinsCraftItemExperienceGem");
		LanguageRegistry.addName(this, "Gem of Experience");
	}
	public String getTextureFile () {
		return BinsCraft.resourceItemIconPath;
	}
	
	@Override
	public int takeCharge(ItemStack itemStack) {
		if(itemStack.getItemDamage() +1 >= this.getMaxDamage()) return 0;
		itemStack.damageItem(1, null);
		return 1;
	}
	
	@Override
	public int takeCharge(ItemStack itemStack, int max) {
		if(itemStack.getItemDamage() + 1 >= this.getMaxDamage() || max < 1) return 0;
		itemStack.damageItem(1, null);
		return 1;
	}
	@Override
	public int giveCharge(ItemStack itemStack, int amount) {
		if(!(itemStack.getItemDamage() - amount - 1 < 0)) {
			itemStack.damageItem(-amount, null);
			return amount;
		}
		return 0;
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player) {
		if(!player.isSneaking())
			player.experience += (1F / 17F) * takeCharge(itemStack, 1);
		else
			player.experience -= (1F / 17F) * giveCharge(itemStack, 1);
		while(player.experience > 1.0F) {
			player.addExperienceLevel(1);
			player.experience -= 1F;
		}
		while(player.experience < 0.0F) {
			player.addExperienceLevel(-1);
			player.experience += 1F;
		}
		return itemStack;
	}
}