package binscraft.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import binscraft.BinsCraft;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class ItemRedstoneLapis extends Item {

	public ItemRedstoneLapis(int id) {
		super(id);
		this.setMaxStackSize(64).
		setCreativeTab(CreativeTabs.tabMaterials).
		setIconIndex(0).
		setItemName("BinsCraftItemRedstoneLapis");
		LanguageRegistry.addName(this, "Redstone And Lapis");
	}
	public String getTextureFile () {
        return BinsCraft.resourceItemIconPath;
	}
}
