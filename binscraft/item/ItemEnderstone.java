package binscraft.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import binscraft.BinsCraft;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class ItemEnderstone extends Item {
	public ItemEnderstone(int id) {
		super(id);
		this.setMaxStackSize(1).
		setCreativeTab(CreativeTabs.tabMisc).
		setIconIndex(6).
		setItemName("BinsCraftItemEnderstone");
		setMaxDamage(5000);
		setCreativeTab(CreativeTabs.tabMisc);
		LanguageRegistry.addName(this, "Enderstone");
	}
	
	public String getTextureFile () {
		return BinsCraft.resourceItemIconPath;
	}
	
    public int getMaxItemUseDuration(ItemStack par1ItemStack)
    {
        return 5000;
    }
	
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player)
	{
		float experienceCost = 0.05F;
		if(player.experience < experienceCost && player.experienceLevel < 1)
			return itemStack;

		float var4 = 1.0F;
		float var5 = player.prevRotationPitch + (player.rotationPitch - player.prevRotationPitch) * var4;
		float var6 = player.prevRotationYaw + (player.rotationYaw - player.prevRotationYaw) * var4;
		double var7 = player.prevPosX + (player.posX - player.prevPosX) * (double)var4;
		double var9 = player.prevPosY + (player.posY - player.prevPosY) * (double)var4 + 1.62D - (double)player.yOffset;
		double var11 = player.prevPosZ + (player.posZ - player.prevPosZ) * (double)var4;
		Vec3 var13 = world.getWorldVec3Pool().getVecFromPool(var7, var9, var11);
		float var14 = MathHelper.cos(-var6 * 0.017453292F - (float)Math.PI);
		float var15 = MathHelper.sin(-var6 * 0.017453292F - (float)Math.PI);
		float var16 = -MathHelper.cos(-var5 * 0.017453292F);
		float var17 = MathHelper.sin(-var5 * 0.017453292F);
		float var18 = var15 * var16;
		float var20 = var14 * var16;
		double range = 150.0D;
		Vec3 var23 = var13.addVector((double)var18 * range, (double)var17 * range, (double)var20 * range);
		MovingObjectPosition position = world.rayTraceBlocks_do_do(var13, var23, true, false);
		
		if(position == null)
			return itemStack;
		double deltaY = 0.0;
		double deltaX = 0.50;
		double deltaZ = 0.50;
		
		if(position.sideHit == 0) {
			deltaY -= 0.180;
		}
		else if(position.sideHit == 1) {
			deltaY += 3;
			deltaY -= 0.379;
		}
		else if(position.sideHit == 2) {
			deltaZ -= 1;
			deltaY += 1;
			deltaY -= 0.379;
		}
		else if(position.sideHit == 3) {
			deltaZ += 1;
			deltaY += 1;
			deltaY -= 0.379;
		}
		else if(position.sideHit == 4) {
			deltaX -= 1;
			deltaY += 1;
			deltaY -= 0.379;
		}
		else if(position.sideHit == 5) {
			deltaX += 1;
			deltaY += 1;
			deltaY -= 0.379;
		}
		world.playSoundAtEntity(player, "mob.endermen.portal", 1.0F, 1F);
		player.setPosition(position.blockX + deltaX, position.blockY + deltaY, position.blockZ + deltaZ);
		world.playSoundAtEntity(player, "mob.endermen.portal", 1.0F, 1.0F);

		player.experience -= experienceCost;
		if(player.experience < 0) {
			player.addExperienceLevel(-1);
			player.experience += 1.0F;
		}
		itemStack.damageItem(20, player);
		return itemStack;
	}
}
