package binscraft.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import binscraft.BinsCraft;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class ItemHealStone extends Item {
	public ItemHealStone(int id) {
		super(id);
		this.setMaxStackSize(64).
		setCreativeTab(CreativeTabs.tabMisc).
		setIconIndex(4).
		setItemName("BinsCraftItemHealStone");
		LanguageRegistry.addName(this, "Gem of Health");
	}
	
	public String getTextureFile () {
		return BinsCraft.resourceItemIconPath;
	}
	
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer player)
	{
		int toHeal = player.getMaxHealth() - player.getHealth();
		
		if(player.experienceLevel < toHeal / 2) {
			toHeal = player.experienceLevel * 2;
		}
		player.setEntityHealth(player.getHealth() + toHeal);
		player.addExperienceLevel(-toHeal / 2);
		return par1ItemStack;
	}
}
