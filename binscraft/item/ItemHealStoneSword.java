package binscraft.item;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumToolMaterial;
import net.minecraft.item.ItemSword;
import binscraft.BinsCraft;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class ItemHealStoneSword extends ItemSword {

	protected EnumToolMaterial toolMaterial;
	protected int weaponDamage;

	public ItemHealStoneSword(int id) {
		super(id, EnumToolMaterial.EMERALD);
		this.toolMaterial = EnumToolMaterial.EMERALD;
        this.maxStackSize = 1;
        this.setMaxDamage(EnumToolMaterial.EMERALD.getMaxUses());
        this.setCreativeTab(CreativeTabs.tabCombat);
        this.weaponDamage = EnumToolMaterial.EMERALD.getDamageVsEntity();
		setIconIndex(5).
		setItemName("BinsCraftItemHealStoneSword");
		LanguageRegistry.addName(this, "Gem of Health Sword");
	}
	public String getTextureFile () {
		return BinsCraft.resourceItemIconPath;
	}
}
