package binscraft.inventory;

import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.common.ISidedInventory;
import binscraft.BinsCraft;
import binscraft.IExperienceChargable;
import binscraft.IExperienceContainable;
import binscraft.Utils;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileEntityExperienceGenerator extends TileEntity implements ISidedInventory {
	private ItemStack[] inventory;
	public int burnTime = 0;
	public int burnTimeMaxItem = 0;
	public int cookPoints = 0;
	public int sync = 0;
	
	public TileEntityExperienceGenerator() {
		inventory = new ItemStack[2];
	}
	
	@Override
	public int getSizeInventory() {
		return inventory.length;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return inventory[index];
	}

	@Override
	public ItemStack decrStackSize(int slotIndex, int amount) {
		ItemStack stack = getStackInSlot(slotIndex);
		if(stack != null){
			if(stack.stackSize <= amount){
				setInventorySlotContents(slotIndex, null);
			} else{
				stack = stack.splitStack(amount);
				if(stack.stackSize == 0){
					setInventorySlotContents(slotIndex, null);
				}
			}
		}
		return stack;
	}
	@Override
	public ItemStack getStackInSlotOnClosing(int slotIndex) {
		ItemStack stack = getStackInSlot(slotIndex);
		if(stack != null){
			setInventorySlotContents(slotIndex, null);
		}
		return stack;
	}
	
	@Override
	public void setInventorySlotContents(int slot, ItemStack stack) {
		inventory[slot] = stack;
		if(stack != null && stack.stackSize > getInventoryStackLimit()){
			stack.stackSize = getInventoryStackLimit();
		}
	}

	@Override
	public String getInvName() {
		return "BinsCraftTileEntity";
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
			return worldObj.getBlockTileEntity(xCoord, yCoord, zCoord) == this &&
			player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
	}

	@Override
	public void openChest() {}

	@Override
	public void closeChest() {}
	
	@Override
	public void readFromNBT(NBTTagCompound tagCompound){
		super.readFromNBT(tagCompound);
		NBTTagList tagList = tagCompound.getTagList("Inventory");
		this.inventory = new ItemStack[this.getSizeInventory()];
		for(int i = 0; i < tagList.tagCount(); i++){
			NBTTagCompound tag = (NBTTagCompound) tagList.tagAt(i);
			byte slot = tag.getByte("Slot");
			if(slot >= 0 && slot < inventory.length){
				inventory[slot] = ItemStack.loadItemStackFromNBT(tag);
			}
		}
		this.burnTime = tagCompound.getShort("burnTime");
		this.cookPoints = tagCompound.getShort("cookPoints");
		this.burnTimeMaxItem = tagCompound.getShort("burnTimeMaxItem");
	}
	
	@Override
	public void writeToNBT(NBTTagCompound tagCompound){
		super.writeToNBT(tagCompound);
		tagCompound.setShort("burnTime", (short)this.burnTime);
		tagCompound.setShort("cookPoints", (short)this.cookPoints);
		tagCompound.setShort("burnTimeMaxItem", (short)this.burnTimeMaxItem);
		NBTTagList itemList = new NBTTagList();
		for(int i = 0; i < inventory.length; i++){
			ItemStack stack = inventory[i];
			if(stack != null){
				NBTTagCompound tag = new NBTTagCompound();
				tag.setByte("Slot", (byte) i);
				stack.writeToNBT(tag);
				itemList.appendTag(tag);
			}
		}
		tagCompound.setTag("Inventory", itemList);
	}
	
	@SideOnly(Side.CLIENT)
	public int getCookProgressScaled(int scale)
	{
		return (cookPoints) * scale / 1000;
	}
	
	@SideOnly(Side.CLIENT)
	public int getBurnTimeRemainingScaled(int scale)
	{
		if(burnTimeMaxItem != 0) {
			return this.burnTime * scale / this.burnTimeMaxItem;
		}
		return 0;
	}
	
	public boolean isBurning()
	{
		return burnTime > 0;
	}
	
	public void updateEntity()
	{
		boolean burning = this.burnTime > 0;
		boolean burnEngange = false;
		if (this.burnTime > 0) this.burnTime--;

		if (!this.worldObj.isRemote)
		{
			sync++;
			if(sync >= 10) sync();

			if (this.burnTime <= 0)
			{
				this.burnTimeMaxItem = this.burnTime = Utils.getBurnTime(this.inventory[0]);
				if (burnTime > 0)
				{
					if (inventory[0] != null)
					{
						
						inventory[0].stackSize--;
						if (inventory[0].stackSize == 0) 
							inventory[0] = this.inventory[0].getItem().getContainerItemStack(inventory[0]);
					}
				}
			}

			if (isBurning())
			{
				cookPoints+=10;
				while(this.cookPoints >= 1000)
				{
					this.cookPoints -= 1000;
					if(canEmitCharge())
						((IExperienceChargable)inventory[1].getItem()).giveCharge(inventory[1], 1);
					else this.worldObj.spawnEntityInWorld(new EntityXPOrb(this.worldObj, this.xCoord, this.yCoord, this.zCoord, 1));
				}	
			}
			else
			{
				this.cookPoints = 0;
			}

			if (burning != this.burnTime > 0)
			{
				worldObj.markBlockForRenderUpdate(xCoord, yCoord, zCoord);
				System.out.println("Update!");
			}
		}
	}
	
	protected boolean canEmitCharge() {
		if(inventory[1] != null && inventory[1].getItem() instanceof IExperienceContainable)
			return true;
		return false;
	}
	private int getCookPoints(ItemStack itemStack) {
		if(itemStack == null) {
			return 0;
		}
		else if(itemStack.itemID == BinsCraft.ItemPurplePowder.itemID) {
			return 50;
		}
		else if(itemStack.itemID == BinsCraft.ItemPurplePowderBunch.itemID) {
			return 150;
		}
		else if(itemStack.itemID == Item.diamond.itemID) {
			return 1000;
		}
		else if(itemStack.itemID == Item.coal.itemID) {
			return 10;
		}
		else if(itemStack.itemID == Item.bucketLava.itemID) {
			return 1;
		}
		return 0;
	}
	
	public void generateXP()
	{
		if(inventory[1] != null) {
			if(inventory[1].isItemStackDamageable() && inventory[1].getItemDamage() >= 1) {
				if(inventory[1].getItemDamage() < 3) {
					inventory[1].damageItem(-inventory[1].getItemDamage(), null);
					return;
				}
				inventory[1].damageItem(-3, null);
				return;
			}
		}
		worldObj.spawnEntityInWorld(new EntityXPOrb(this.worldObj, this.xCoord, this.yCoord + 1, this.zCoord, 1));
	}
	public void sync() {
		PacketDispatcher.sendPacketToAllInDimension(getDescriptionPacket(), worldObj.provider.dimensionId);
	}
	
	@Override
	public void onDataPacket(INetworkManager net, Packet132TileEntityData pkt) {
		this.burnTime = pkt.customParam1.getShort("burnTime");
		this.cookPoints = pkt.customParam1.getShort("cookPoints");
		this.burnTimeMaxItem = pkt.customParam1.getShort("burnTimeMaxItem");
	}
	
	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound tagCompound = new NBTTagCompound();
		tagCompound.setShort("burnTime", (short)this.burnTime);
		tagCompound.setShort("cookPoints", (short)this.cookPoints);
		tagCompound.setShort("burnTimeMaxItem", (short)this.burnTimeMaxItem);
		return new Packet132TileEntityData(this.xCoord, this.yCoord, this.zCoord, 1, tagCompound);
	}

	@Override
	public int getStartInventorySide(ForgeDirection side) {
		if (side == ForgeDirection.DOWN) return 1;
		if (side == ForgeDirection.UP) return 0;
		return 2;
	}

	@Override
	public int getSizeInventorySide(ForgeDirection side) {
		return 1;
	}
}
