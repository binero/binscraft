package binscraft.inventory;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.common.ISidedInventory;
import binscraft.BinsCraft;
import binscraft.Utils;
import cpw.mods.fml.common.network.PacketDispatcher;

public class TileEntityExtractor extends TileEntity implements ISidedInventory {
	private ItemStack[] inventory = new ItemStack[5];
	public int burnTime = 0;
	public int progress = 0;
	public int burnTimeMax = 0;
	public int sync = 0;
	public boolean dropInventory = true;
	protected Random random = new Random(System.currentTimeMillis());
	
	@Override
	public int getSizeInventory() {
		return this.inventory.length;
	}
	
	@Override
	public ItemStack getStackInSlot(int slotIndex) {
		return inventory[slotIndex];
	}

	@Override
	public ItemStack decrStackSize(int slotIndex, int amount) {
		ItemStack stack = getStackInSlot(slotIndex);
		if(stack != null){
			if(stack.stackSize <= amount){
				setInventorySlotContents(slotIndex, null);
			} else {
				stack = stack.splitStack(amount);
				if(stack.stackSize == 0){
					setInventorySlotContents(slotIndex, null);
				}
			}
		}
		return stack;
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int slotIndex) {
		ItemStack stack = getStackInSlot(slotIndex);
		if(stack != null){
			setInventorySlotContents(slotIndex, null);
		}
		return stack;
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack stack) {
		inventory[slot] = stack;
		if(stack != null && stack.stackSize > getInventoryStackLimit()){
			stack.stackSize = getInventoryStackLimit();
		}
	}

	@Override
	public void onInventoryChanged() {
		if(!canExtract()) progress = 0;
	}
	
	@Override
	public void updateEntity() {
		boolean burning = burnTime > 0;
		boolean canExtract = canExtract();
		if(burnTime > 0) burnTime--;
		if(inventory[2] != null && burnTime <= 0 && canExtract) {
			int fuelTime = Utils.getBurnTime(inventory[2]);
			if(fuelTime > 0) {
				this.burnTime += fuelTime;
				burnTimeMax = fuelTime;
				this.inventory[2].stackSize--;
				if(inventory[2].stackSize <= 0) inventory[2] = null;
			}
		}
		else if(burnTime <= 0) progress = 0;
		else if(!canExtract) progress = 0;
		else if(canExtract) {
			progress++;
			if(progress >= 100) {
				if(random.nextInt() % 3 == 0) {
					if(inventory[0] == null) inventory[0] = new ItemStack(BinsCraft.ItemEssence);
					else inventory[0].stackSize++;
				} else if(random.nextInt() % 10 == 0) {
					if(inventory[1] == null) inventory[1] = new ItemStack(BinsCraft.ItemWaste);
					else if(inventory[1].itemID == BinsCraft.ItemWaste.itemID && inventory[1].stackSize < inventory[1].getMaxStackSize()) inventory[1].stackSize++;
				}
				progress-=100;
				inventory[3].stackSize--;
				if(inventory[3].stackSize <= 0) inventory[3] = null;
			}
		}
		sync++;
		if(sync >= 10) {
			sync-=10;
			sync();
		}
		if(burning != burnTime > 0) {
			dropInventory = false;
			int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
			if (burnTime > 0) worldObj.setBlockAndMetadataWithNotify(xCoord, yCoord, zCoord, BinsCraft.BlockExtractorActive.blockID, meta);
			else worldObj.setBlockAndMetadataWithNotify(xCoord, yCoord, zCoord, BinsCraft.BlockExtractorInactive.blockID, meta);
			dropInventory = true;
			validate();
	        Chunk chunk = worldObj.getChunkFromChunkCoords(xCoord >> 4, zCoord >> 4);
	        if (chunk != null) chunk.setChunkBlockTileEntity(xCoord & 15, yCoord, zCoord & 15, this);
	    }
	}
	private boolean canExtract() {
		if(inventory[3] == null) return false;
		if	(
				(inventory[3].getItem().itemID == BinsCraft.ItemPurplePowder.itemID) && 
				(inventory[0] == null || (inventory[0].getItem().itemID == BinsCraft.ItemEssence.itemID && inventory[0].stackSize < inventory[0].getItem().getItemStackLimit()))
			)
			return true;
		return false;
	}

	public int getBurnTimeScaled(int scale) {
		if(burnTimeMax == 0) return 0;
		return Math.round((float) burnTime * scale / burnTimeMax);
	}

	public int getProgressScaled(int scale) {
		return progress * scale / 100;
	}
	@Override
	public String getInvName() {
		return "Experience Gemminator";
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return true;
	}

	@Override
	public void openChest() {		
	}

	@Override
	public void closeChest() {		
	}

	@Override
	public int getStartInventorySide(ForgeDirection side) {
		return 0;
	}

	@Override
	public int getSizeInventorySide(ForgeDirection side) {
		return 1;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound tagCompound){
		super.readFromNBT(tagCompound);
		this.burnTime = tagCompound.getInteger("burnTime");
		this.burnTimeMax = tagCompound.getInteger("burnTimeMax");
		this.progress = tagCompound.getInteger("progress");
		NBTTagList tagList = tagCompound.getTagList("Inventory");
		this.inventory = new ItemStack[this.getSizeInventory()];
		for(int i = 0; i < tagList.tagCount(); i++){
			NBTTagCompound tag = (NBTTagCompound) tagList.tagAt(i);
			byte slot = tag.getByte("Slot");
			if(slot >= 0 && slot < inventory.length){
				inventory[slot] = ItemStack.loadItemStackFromNBT(tag);
			}
		}
	}
	
	@Override
	public void writeToNBT(NBTTagCompound tagCompound){
		super.writeToNBT(tagCompound);
		tagCompound.setInteger("burnTime", this.burnTime);
		tagCompound.setInteger("burnTimeMax", this.burnTimeMax);
		tagCompound.setInteger("progress", this.progress);
		NBTTagList itemList = new NBTTagList();
		for(int i = 0; i < inventory.length; i++){
			ItemStack stack = inventory[i];
			if(stack != null){
				NBTTagCompound tag = new NBTTagCompound();
				tag.setByte("Slot", (byte) i);
				stack.writeToNBT(tag);
				itemList.appendTag(tag);
			}
		}
		tagCompound.setTag("Inventory", itemList);
	}
	
	public void sync() {
		PacketDispatcher.sendPacketToAllInDimension(getDescriptionPacket(), worldObj.provider.dimensionId);
	}
	
	@Override
	public void onDataPacket(INetworkManager net, Packet132TileEntityData pkt) {
		this.burnTime = pkt.customParam1.getInteger("burnTime");
		this.burnTimeMax = pkt.customParam1.getInteger("burnTimeMax");
		this.progress = pkt.customParam1.getInteger("progress");
	}
	
	@Override
	public Packet getDescriptionPacket() {
		NBTTagCompound tagCompound = new NBTTagCompound();
		super.writeToNBT(tagCompound);
		tagCompound.setInteger("burnTime", this.burnTime);
		tagCompound.setInteger("burnTimeMax", this.burnTimeMax);
		tagCompound.setInteger("progress", this.progress);
		return new Packet132TileEntityData(this.xCoord, this.yCoord, this.zCoord, 1, tagCompound);
	}
}