package binscraft;

import net.minecraft.item.ItemStack;

public interface IExperienceChargable {
	public int giveCharge(ItemStack itemStack, int amount);
}
