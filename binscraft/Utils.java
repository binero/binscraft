package binscraft;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import cpw.mods.fml.common.registry.GameRegistry;

public class Utils {
	public static int getBurnTime(ItemStack itemStack) {
		if (itemStack == null)
		{
			return 0;
		}
		else
		{
			int itemID = itemStack.getItem().itemID;
			Item item = itemStack.getItem();
			if (itemStack.getItem() instanceof ItemBlock && Block.blocksList[itemID] != null)
			{
				Block var3 = Block.blocksList[itemID];
				if (var3 == Block.woodSingleSlab)
				{
					return 150;
				}
				if (var3.blockMaterial == Material.wood)
				{
					return 300;
				}
			}

			if (item instanceof ItemTool && ((ItemTool) item).getToolMaterialName().equals("WOOD")) return 200;
			if (item instanceof ItemSword && ((ItemSword) item).func_77825_f().equals("WOOD")) return 200;
			if (item instanceof ItemHoe && ((ItemHoe) item).func_77842_f().equals("WOOD")) return 200;
			if (itemID == Item.stick.itemID) return 100;
			if (itemID == Item.coal.itemID) return 1600;
			if (itemID == Item.bucketLava.itemID) return 20000;
			if (itemID == Block.sapling.blockID) return 100;
			if (itemID == Item.blazeRod.itemID) return 2400;
			return GameRegistry.getFuelValue(itemStack);
		}
	}}
