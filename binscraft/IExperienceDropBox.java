package binscraft;

import net.minecraft.inventory.IInventory;

public interface IExperienceDropBox extends IInventory {
	public int getExperienceLevel();
	public int pushExperience(int ammount);
}
