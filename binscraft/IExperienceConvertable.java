package binscraft;

public interface IExperienceConvertable extends IExperienceContainable {
	public int experienceValue = 0;
}
