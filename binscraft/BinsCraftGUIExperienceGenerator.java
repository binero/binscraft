package binscraft;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

import binscraft.gui.ContainerExperienceGenerator;
import binscraft.inventory.TileEntityExperienceGenerator;

public class BinsCraftGUIExperienceGenerator extends GuiContainer {

	public TileEntityExperienceGenerator tileEntity;
	
	public BinsCraftGUIExperienceGenerator (InventoryPlayer inventoryPlayer, TileEntityExperienceGenerator tileEntity) {
		super(new ContainerExperienceGenerator(tileEntity, inventoryPlayer));
		this.tileEntity = tileEntity;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int param1, int param2) {
		fontRenderer.drawString("Experience Generator", 6, 6, 0xFFFFFF);
		fontRenderer.drawString(StatCollector.translateToLocal("container.inventory"), 6, ySize - 96, 0xFFFFFF);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		int texture = mc.renderEngine.getTexture(BinsCraft.resourcePath + "ExperienceGeneratorGUI.png");
		this.mc.renderEngine.bindTexture(texture);
		int x = (width - xSize) / 2;
		int y = (height - ySize) / 2;
		this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
		int remaining;
		if (this.tileEntity.isBurning())
		{
			remaining = this.tileEntity.getBurnTimeRemainingScaled(12);
			this.drawTexturedModalRect(x + 56, y + 36 + 12 - remaining, 176, 12 - remaining, 14, remaining + 2);
		}
		remaining = this.tileEntity.getCookProgressScaled(24);
		this.drawTexturedModalRect(x + 79, y + 34, 176, 14, remaining + 1, 16);
	}

}
