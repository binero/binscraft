package binscraft;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import binscraft.block.BlockExperienceGenerator;
import binscraft.block.BlockExtractorActive;
import binscraft.block.BlockExtractorInactive;
import binscraft.gui.GuiHandler;
import binscraft.inventory.TileEntityExperienceGenerator;
import binscraft.inventory.TileEntityExtractor;
import binscraft.item.ItemEnderstone;
import binscraft.item.ItemEssence;
import binscraft.item.ItemExperienceGem;
import binscraft.item.ItemHealStone;
import binscraft.item.ItemHealStoneSword;
import binscraft.item.ItemPurplePowder;
import binscraft.item.ItemPurplePowderBunch;
import binscraft.item.ItemRedstoneLapis;
import binscraft.item.ItemWaste;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid="BinsCraft", name="BinsCraft", version="0.0.3")
@NetworkMod(clientSideRequired=true, serverSideRequired=true)
public class BinsCraft {
	
	@Instance
	public static BinsCraft instance = new BinsCraft();
	
	public static String resourcePath = "/binscraft/resources/";
	public static String resourceItemIconPath = resourcePath + "items.png";
	public static String resourceBlockTexturePath = resourcePath + "blocks.png";
	public static Item ItemRedstoneLapis;
	public static Item ItemPurplePowder;
	public static Item ItemPurplePowderBunch;
	public static Item ItemExperienceGem;
	public static Item ItemHealStone;
	public static Item ItemHealStoneSword;
	public static Item ItemEnderstone;
	public static Block BlockExperienceGenerator;
	public static Block BlockExtractorInactive;
	public static Block BlockExtractorActive;
	public static Item ItemEssence;
	public static Item ItemWaste;
	
	public static int itemRedstoneLapisID;
	public static int itemPurplePowderID;
	public static int itemPuplePowderBunchID;
	public static int itemExperienceGemID;
	public static int itemHealStoneID;
	public static int itemHealStoneSwordID;
	public static int itemEnderstoneID;
	public static int blockExperienceGeneratorID;
	public static int BlockExtractorInactiveID;
	public static int BlockExtractorActiveID;
	public static int BlockGemmenatorhealthID;
	public static int ItemEssenceID;
	public static int ItemWasteID;
		
	@PreInit
	public void preInit(FMLPreInitializationEvent event) {
		System.out.println("BinsCraft Preinitialize");
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();
		itemRedstoneLapisID = config.getItem("Redstone and Lapis", 11015).getInt();
		itemPurplePowderID = config.getItem("Purple Powder", 11016).getInt();
		itemPuplePowderBunchID = config.getItem("Lots of Purple Powder", 11017).getInt();
		itemExperienceGemID = config.getItem("Gem of Experience", 11018).getInt();
		itemHealStoneID = config.getItem("Gem of Health", 11019).getInt();
		itemHealStoneSwordID = config.getItem("Gem of Health Sword", 11020).getInt();
		itemEnderstoneID = config.getItem("Enderstone", 11021).getInt();
		blockExperienceGeneratorID = config.getBlock("Experience Generator", 2045).getInt();
		BlockExtractorInactiveID = config.getBlock("Extractor", 2046).getInt();
		BlockExtractorActiveID = config.getBlock("Extractor Active", 2047).getInt();
		BlockGemmenatorhealthID = config.getBlock("Health Gemmenator", 2048).getInt();
		ItemEssenceID = config.getBlock("Experience Essence", 2049).getInt();
		ItemWasteID = config.getBlock("Waste", 2050).getInt();
		config.save();
	}
	
	@Init
	public void load(FMLInitializationEvent event) {
		System.out.println("BinsCraft Initialize");
		this.initializeBlocksItems();
		this.initializeRecipes();
		MinecraftForge.EVENT_BUS.register(new BinsCraft());
		GuiHandler guiHandler = new GuiHandler();
		NetworkRegistry.instance().registerGuiHandler(instance, guiHandler);
		GameRegistry.registerTileEntity(TileEntityExperienceGenerator.class, "TileEntityExperienceGenerator");
		GameRegistry.registerTileEntity(TileEntityExtractor.class, "BinsCraftTileEntityGemmenator");
		MinecraftForgeClient.preloadTexture(resourceBlockTexturePath);
	}
	private void initializeBlocksItems() {
		ItemRedstoneLapis = new ItemRedstoneLapis(this.itemRedstoneLapisID);;
		ItemPurplePowder = new ItemPurplePowder(this.itemPurplePowderID);;
		ItemPurplePowderBunch = new ItemPurplePowderBunch(this.itemPuplePowderBunchID);;
		ItemExperienceGem = new ItemExperienceGem(this.itemExperienceGemID);;
		ItemHealStone = new ItemHealStone(this.itemHealStoneID);
		ItemHealStoneSword = new ItemHealStoneSword(this.itemHealStoneSwordID);
		ItemEnderstone = new ItemEnderstone(this.itemEnderstoneID);
		BlockExperienceGenerator = new BlockExperienceGenerator(this.blockExperienceGeneratorID, 0, Material.rock);
		ItemEssence = new ItemEssence(ItemEssenceID);
		BlockExtractorInactive = new BlockExtractorInactive(BlockExtractorInactiveID);
		BlockExtractorActive = new BlockExtractorActive(BlockExtractorActiveID);
		ItemWaste = new ItemWaste(ItemWasteID);
	}
	
	private void initializeRecipes() {
		ItemStack redstoneLapisStack = new ItemStack(this.ItemRedstoneLapis);
		ItemStack purplePowderStack = new ItemStack(this.ItemPurplePowder);
		ItemStack purplePowderStackx9 = new ItemStack(this.ItemPurplePowder, 9, 0);
		ItemStack purplePowderBunchStack = new ItemStack(this.ItemPurplePowderBunch);
		ItemStack experienceGemStack = new ItemStack(this.ItemExperienceGem, 1, ItemExperienceGem.getMaxDamage());
		ItemStack experienceGeneratorStack = new ItemStack(this.BlockExperienceGenerator);
		ItemStack healStoneStack = new ItemStack(this.ItemHealStone);
		ItemStack enderstoneStack = new ItemStack(this.ItemEnderstone);
		ItemStack healStoneSwordStack = new ItemStack(this.ItemHealStoneSword);
		ItemStack extractor = new ItemStack(this.BlockExtractorInactive);
		ItemStack essenceStack = new ItemStack(this.ItemEssence);
		ItemStack emeraldBlockStack = new ItemStack(Block.blockEmerald);
		ItemStack redstoneStack = new ItemStack(Item.redstone);
		ItemStack lapisStack = new ItemStack(Item.dyePowder, 1, 4);
		ItemStack diamondStack = new ItemStack(Item.diamond);
		ItemStack glinsteringMelonStack = new ItemStack(Item.speckledMelon);
		ItemStack enderPearlStack = new ItemStack(Item.enderPearl);
		ItemStack stickStack = new ItemStack(Item.stick);
		ItemStack goldBlockStack = new ItemStack(Block.blockGold);
		ItemStack ironBarStack = new ItemStack(Item.ingotIron);
		ItemStack pistonStack = new ItemStack(Block.pistonBase);
		ItemStack furnaceStack = new ItemStack(Block.stoneOvenIdle);
		ItemStack obsidianStack = new ItemStack(Block.obsidian);
		
		GameRegistry.addSmelting(this.ItemRedstoneLapis.itemID, purplePowderStack, 5F);
		GameRegistry.addShapelessRecipe(redstoneLapisStack, 
				redstoneStack, lapisStack);
		GameRegistry.addShapelessRecipe(purplePowderBunchStack, 
				purplePowderStack, purplePowderStack ,purplePowderStack,
				purplePowderStack, purplePowderStack, purplePowderStack,
				purplePowderStack, purplePowderStack, purplePowderStack);
		GameRegistry.addShapelessRecipe(purplePowderStackx9, 
				purplePowderBunchStack);
		GameRegistry.addRecipe(extractor, "aba", "cdc", "efe",
				'a', purplePowderBunchStack, 'b', ironBarStack, 'c', pistonStack, 'd', goldBlockStack, 'e', diamondStack, 'f', furnaceStack);
		GameRegistry.addRecipe(experienceGemStack, "xxx", "xyx", "xxx",
				'x', essenceStack, 'y', diamondStack);
		GameRegistry.addRecipe(experienceGeneratorStack, "xzx", "zyz", "xzx",
				'x', obsidianStack, 'y', experienceGemStack, 'z', essenceStack);
		GameRegistry.addRecipe(healStoneStack, "xyx", "yzy", "xyx",
				'z', experienceGemStack, 'y', purplePowderBunchStack, 'x', glinsteringMelonStack);
		GameRegistry.addRecipe(enderstoneStack, "xyx", "yzy", "xyx",
				'z', experienceGemStack, 'y', purplePowderBunchStack, 'x', enderPearlStack);
		GameRegistry.addRecipe(healStoneSwordStack, "y", "y", "x", 
				'y', healStoneStack, 'x', stickStack);
	}
	
	@ForgeSubscribe
	public void OnPlayerDeath(LivingDeathEvent event) {
		if(event.entity instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) event.entity;
			if(player.inventory.mainInventory[player.inventory.currentItem] == null ||player.inventory.mainInventory[player.inventory.currentItem].itemID != ItemHealStoneSword.itemID)
				return;
			int toHeal = player.experienceLevel * 2;
			if (toHeal < 1)
				return;
			else if(toHeal > player.getMaxHealth()) {
				toHeal = player.getMaxHealth();
			}
			player.setEntityHealth(toHeal);
			player.addExperienceLevel(-(toHeal / 2));
			event.setCanceled(true);
		}
	}
}