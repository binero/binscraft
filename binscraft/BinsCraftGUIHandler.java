package binscraft;

import binscraft.gui.ContainerExperienceGenerator;
import binscraft.inventory.TileEntityExperienceGenerator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;

public class BinsCraftGUIHandler implements IGuiHandler {
	@Override
	public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
			TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
			if(tileEntity instanceof TileEntityExperienceGenerator){
				return new ContainerExperienceGenerator((TileEntityExperienceGenerator) tileEntity, player.inventory);
			}
			return null;
	}

	@Override
	public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		if(tileEntity instanceof TileEntityExperienceGenerator){
				return new BinsCraftGUIExperienceGenerator(player.inventory, (TileEntityExperienceGenerator) tileEntity);
		}
		return null;
	}
}
