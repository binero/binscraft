package binscraft.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import binscraft.IExperienceConvertable;
import binscraft.IExperienceDropBox;

public class SlotExperiencePush extends Slot {

	public SlotExperiencePush(IExperienceDropBox inventory, int id, int x, int y) {
		super((IInventory) inventory, id, x, y);
	}
	
	@Override
	public boolean isItemValid(ItemStack itemStack) {
		if(itemStack.getItem() instanceof IExperienceConvertable) return true;
		return false;
	}
}
