package binscraft.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import binscraft.inventory.TileEntityExtractor;

public class ContainerExtractor extends ContainerSmart {
	protected TileEntityExtractor tileEntity;
	
	public ContainerExtractor(TileEntityExtractor tileEntity, InventoryPlayer playerInventory) {
		this.tileEntity = tileEntity;
		addSlotToContainer(new SlotResult(tileEntity, 0, 105, 35));
		addSlotToContainer(new SlotResult(tileEntity, 1, 144, 35));
		addSlotToContainer(new SlotFuel(tileEntity, 2, 12, 35));
		addSlotToContainer(new SlotPurplePowder(tileEntity, 3, 45, 35));
		bindPlayerInventory(playerInventory);

	}

	protected void bindPlayerInventory(InventoryPlayer playerInventory) {
		for (int row = 0; row < 3; row++) {
			for (int slot = 0; slot < 9; slot++) {
				addSlotToContainer(new Slot(playerInventory, slot + row * 9 + 9, 8 + slot * 18, 84 + row * 18));
			}
		}
        for (int slot = 0; slot < 9; slot++) {
            addSlotToContainer(new Slot(playerInventory, slot, 8 + slot * 18, 142));
        }
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return tileEntity.isUseableByPlayer(player);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
		ItemStack stack = null;
		Slot slotObject = (Slot) inventorySlots.get(slot);
		if (slotObject != null && slotObject.getHasStack()) {
			ItemStack stackInSlot = slotObject.getStack();
			stack = stackInSlot.copy();
			if(slot <= 4) {
				if(!mergeItemStack(stackInSlot, 5, this.inventorySlots.size(), true)) return null;
			} else {
				if(mergeItemStack(stackInSlot, 0, 4, false));
				else if(slot <= inventorySlots.size() - 9) {
					System.out.println("test");
					if(!mergeItemStack(stackInSlot, inventorySlots.size() - 9, inventorySlots.size(), true)) return null;
				}
				else if(!mergeItemStack(stackInSlot, 4, inventorySlots.size() - 9, false)) return null;
				else return null;
			}
			if (stackInSlot.stackSize == 0) {
				 slotObject.putStack(null);
			} else {
				 slotObject.onSlotChanged();
			}
		}
		return stack;
	}
}
