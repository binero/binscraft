package binscraft.gui;

import binscraft.BinsCraft;
import binscraft.inventory.TileEntityExperienceGenerator;
import binscraft.inventory.TileEntityExtractor;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.StatCollector;

public class GuiExperienceGenerator extends GuiContainer {
	protected TileEntityExperienceGenerator tileEntity;

	public GuiExperienceGenerator (TileEntityExperienceGenerator tileEntity, InventoryPlayer inventoryPlayer) {
		super(new ContainerExperienceGenerator(tileEntity, inventoryPlayer));
		this.tileEntity = tileEntity;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
		int texture = mc.renderEngine.getTexture(BinsCraft.resourcePath + "GuiExperienceGenerator.png");
		this.mc.renderEngine.bindTexture(texture);
		int x = (width - xSize) / 2;
		int y = (height - ySize) / 2;
		this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
		int experience = this.tileEntity.getBurnTimeRemainingScaled(14);
		int progress = this.tileEntity.getCookProgressScaled(24);
		this.drawTexturedModalRect(x + 56, y + 14 + 36 - experience, 176, 14-experience, 16, experience);
		this.drawTexturedModalRect(x + 80, y + 35, 176, 14, progress, 17);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int param1, int param2) {
		fontRenderer.drawString("Experience Generator", 6, 6, 4210752);
		fontRenderer.drawString(StatCollector.translateToLocal("container.inventory"), 6, ySize - 96, 4210752);
	}
}
