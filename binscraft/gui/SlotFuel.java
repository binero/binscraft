package binscraft.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import binscraft.Utils;

public class SlotFuel extends Slot {

	public SlotFuel(IInventory inventory, int id, int x, int y) {
		super(inventory, id, x, y);
	}
	
	@Override
	public boolean isItemValid(ItemStack itemStack) {
		if(Utils.getBurnTime(itemStack) <= 0) return false;
		return true;
	}
}