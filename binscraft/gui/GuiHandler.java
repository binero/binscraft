package binscraft.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import binscraft.inventory.TileEntityExperienceGenerator;
import binscraft.inventory.TileEntityExtractor;
import cpw.mods.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		if(ID == 0) return new ContainerExtractor((TileEntityExtractor) tileEntity, player.inventory);
		if(ID == 1) return new ContainerExperienceGenerator((TileEntityExperienceGenerator) tileEntity, player.inventory);
		System.out.println("BinsCraft: Error Creating GuiContainer: False Gui ID");
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		if(ID == 0) return new GuiExtractor((TileEntityExtractor) tileEntity, player.inventory);
		if(ID == 1) return new GuiExperienceGenerator((TileEntityExperienceGenerator) tileEntity, player.inventory);
		System.out.println("BinsCraft: Error Creating GuiContainer: False Gui ID");
		return null;
	}
}
