package binscraft.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import binscraft.BinsCraft;
import binscraft.Utils;

public class SlotPurplePowder extends Slot {

	public SlotPurplePowder(IInventory inventory, int id, int x, int y) {
		super(inventory, id, x, y);
	}
	
	@Override
	public boolean isItemValid(ItemStack itemStack) {
		if(itemStack != null && (itemStack.getItem().itemID == BinsCraft.ItemPurplePowder.itemID || itemStack.getItem().itemID == BinsCraft.ItemPurplePowderBunch.itemID))
			return true;
		return false;
	}
}