package binscraft.gui;

import binscraft.inventory.TileEntityExperienceGenerator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

public class ContainerExperienceGenerator extends Container {
	protected TileEntityExperienceGenerator tileEntity;
	public ContainerExperienceGenerator(TileEntityExperienceGenerator tileEntity, InventoryPlayer playerInventory) {
		this.tileEntity = tileEntity;
		addSlotToContainer(new Slot(tileEntity,  0, 56, 53));
		this.addSlotToContainer(new Slot(tileEntity, 1, 116, 35));
		bindPlayerInventory(playerInventory);
	}
	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return tileEntity.isUseableByPlayer(player);
	}

	protected void bindPlayerInventory(InventoryPlayer playerInventory) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
					addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}
        for (int i = 0; i < 9; i++) {
            addSlotToContainer(new Slot(playerInventory, i, 8 + i * 18, 142));
        }
	}
	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slot) {
		ItemStack stack = null;
		Slot slotObject = (Slot) inventorySlots.get(slot);
		if (slotObject != null && slotObject.getHasStack()) {
				ItemStack stackInSlot = slotObject.getStack();
				stack = stackInSlot.copy();
				if (slot == 0 || slot == 1) {
					 if (!mergeItemStack(stackInSlot, 1, inventorySlots.size(), true)) {
							 return null;
					 }
				} else if (!mergeItemStack(stackInSlot, 0, 1, false)) {
					 return null;
				}
				if (stackInSlot.stackSize == 0) {
					 slotObject.putStack(null);
				} else {
					 slotObject.onSlotChanged();
				}
		}
		return stack;
	}
}
