package binscraft.gui;

import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public abstract class ContainerSmart extends Container {
	
	@Override
	protected boolean mergeItemStack(ItemStack itemStackToPlace, int slotStartID, int slotStopID, boolean reversed) {
		boolean var5 = false;
		int slotCurrentID = slotStartID;
		if (reversed) slotCurrentID = slotStopID - 1;
		Slot slot;
		ItemStack itemStackCurrentSlot;

		if (itemStackToPlace.isStackable())
		{
			while (itemStackToPlace.stackSize > 0 && (!reversed && slotCurrentID < slotStopID || reversed && slotCurrentID >= slotStartID))
			{
				slot = (Slot)this.inventorySlots.get(slotCurrentID);
				itemStackCurrentSlot = slot.getStack();

				if (itemStackCurrentSlot != null && itemStackCurrentSlot.itemID == itemStackToPlace.itemID && (!itemStackToPlace.getHasSubtypes() || itemStackToPlace.getItemDamage() == itemStackCurrentSlot.getItemDamage()) && itemStackToPlace.areItemStackTagsEqual(itemStackToPlace, itemStackCurrentSlot))
				{
					int totalStackSize = itemStackCurrentSlot.stackSize + itemStackToPlace.stackSize;

					if (totalStackSize <= itemStackToPlace.getMaxStackSize())
					{
						itemStackToPlace.stackSize = 0;
						itemStackCurrentSlot.stackSize = totalStackSize;
						slot.onSlotChanged();
						var5 = true;
					}
					else if (itemStackCurrentSlot.stackSize < itemStackToPlace.getMaxStackSize())
					{
						itemStackToPlace.stackSize -= itemStackToPlace.getMaxStackSize() - itemStackCurrentSlot.stackSize;
						itemStackCurrentSlot.stackSize = itemStackToPlace.getMaxStackSize();
						slot.onSlotChanged();
						var5 = true;
					}
				}

				if (reversed) slotCurrentID--;
				else slotCurrentID++;
			}
		}

		if (itemStackToPlace.stackSize > 0)
		{
			if (reversed) slotCurrentID = slotStopID - 1;
			else slotCurrentID = slotStartID;
			while (!reversed && slotCurrentID < slotStopID || reversed && slotCurrentID >= slotStartID)
			{
				slot = (Slot)this.inventorySlots.get(slotCurrentID);
				itemStackCurrentSlot = slot.getStack();
				if (itemStackCurrentSlot == null && slot.isItemValid(itemStackToPlace))
				{
					slot.putStack(itemStackToPlace.copy());
					slot.onSlotChanged();
					itemStackToPlace.stackSize = 0;
					var5 = true;
					break;
				}

				if (reversed) slotCurrentID--;
				else slotCurrentID++;
			}
		}

		return var5;
	}
}
