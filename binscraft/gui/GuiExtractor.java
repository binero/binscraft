package binscraft.gui;

import org.lwjgl.opengl.GL11;

import binscraft.BinsCraft;
import binscraft.inventory.TileEntityExperienceGenerator;
import binscraft.inventory.TileEntityExtractor;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.StatCollector;

public class GuiExtractor extends GuiContainer {
	protected TileEntityExtractor tileEntity;

	public GuiExtractor (TileEntityExtractor tileEntity, InventoryPlayer inventoryPlayer) {
		super(new ContainerExtractor(tileEntity, inventoryPlayer));
		this.tileEntity = tileEntity;
		xSize = 176;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3) {
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		int texture = mc.renderEngine.getTexture(BinsCraft.resourcePath + "GuiExtractor.png");
		this.mc.renderEngine.bindTexture(texture);
		int x = (width - xSize) / 2;
		int y = (height - ySize) / 2;
		this.drawTexturedModalRect(x, y, 0, 0, xSize, ySize);
		int burnTime = tileEntity.getBurnTimeScaled(10);
		this.drawTexturedModalRect(x + 82, y + 22 + 10 - burnTime, 176, 10 - burnTime, 4, burnTime);

		int progress = tileEntity.getProgressScaled(24);
		this.drawTexturedModalRect(x + 72, y + 40, 176, 10, progress, 5);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int param1, int param2) {
		fontRenderer.drawString("Extractor", 5, 5, 4210752);
		fontRenderer.drawString(StatCollector.translateToLocal("container.inventory"), 6, ySize - 96, 4210752);
	}
}
