package binscraft.block;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;
import net.minecraftforge.common.MinecraftForge;
import binscraft.BinsCraft;
import binscraft.inventory.TileEntityExtractor;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockExtractor extends BlockContainer {
	public BlockExtractor(int id) {
		super(id, 3, Material.iron);
		setHardness(1.5F);
		setStepSound(soundStoneFootstep);
		setCreativeTab(CreativeTabs.tabBlock);
		MinecraftForge.setBlockHarvestLevel(this, "pickaxe", 2);
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityExtractor();
	}
	
	public String getTextureFile () {
		return BinsCraft.resourceBlockTexturePath;
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int i, float f, float g, float t) {		
		if(player.isSneaking()) return false;
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		if(tileEntity == null) return false;
		player.openGui(BinsCraft.instance, 0, world, x, y, z);
		return true;
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, int i, int j) {
		dropItems(world, x, y, z);
		super.breakBlock(world, x, y, z, i, j);
	}
	
	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLiving entity)
	{
		if (!world.isRemote)
		{
			double angle = Math.atan2(entity.posZ - z, entity.posX - x) / Math.PI * 180 + 180;
			ForgeDirection direction;
			if (angle < 45 || angle > 315)
				world.setBlockMetadataWithNotify(x, y, z, ForgeDirection.EAST.getOpposite().ordinal());
			else if (angle < 135)
				world.setBlockMetadataWithNotify(x, y, z, ForgeDirection.SOUTH.getOpposite().ordinal());
			else if (angle < 225)
				world.setBlockMetadataWithNotify(x, y, z, ForgeDirection.WEST.getOpposite().ordinal());
			else
				world.setBlockMetadataWithNotify(x, y, z, ForgeDirection.NORTH.getOpposite().ordinal());
		}
	}
	
	public void dropItems(World world, int x, int y, int z) {
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		if(!(tileEntity instanceof TileEntityExtractor)) return;
		TileEntityExtractor inventory = (TileEntityExtractor) tileEntity;
		tileEntity = null;
		
		if(!inventory.dropInventory) return;
		for(int i = 0; i < inventory.getSizeInventory(); i++) {
			ItemStack item = inventory.getStackInSlot(i);
				if(item != null && item.stackSize > 0) {
				EntityItem itemEntity = new EntityItem(world, x, y, z, new ItemStack(item.itemID, item.stackSize, item.getItemDamage()));
				if(item.hasTagCompound()) {
					itemEntity.func_92014_d().setTagCompound((NBTTagCompound) item.getTagCompound().copy());
				}
				float factor = 0.5F;
				world.spawnEntityInWorld(itemEntity);
				item.stackSize = 0;
			}
		}
	}
}
