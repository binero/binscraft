package binscraft.block;

import net.minecraft.world.IBlockAccess;
import binscraft.inventory.TileEntityExtractor;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockExtractorInactive extends BlockExtractor {
	public BlockExtractorInactive(int id) {
		super(id);
		GameRegistry.registerBlock(this, "BinsCraftBlockExtractorInactive");
		setBlockName("BinsCraftBlockExtractorInactive");
		LanguageRegistry.addName(this, "Experience Extractor");
		setLightValue(0.0F);
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public int getBlockTextureFromSideAndMetadata(int side, int meta)
	{
		if(side == meta || (meta == 0 && side == 3)) {
			return blockIndexInTexture;
		}
		if(side == 0) return blockIndexInTexture + 3;
		if(side == 1) return blockIndexInTexture + 2;
		return blockIndexInTexture + 1;
	}
}
