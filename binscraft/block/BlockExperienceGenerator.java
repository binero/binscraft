package binscraft.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import binscraft.BinsCraft;
import binscraft.inventory.TileEntityExperienceGenerator;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockExperienceGenerator extends BlockContainer {
	public BlockExperienceGenerator(int id, int texture, Material material) {
		super(id, texture, material);
		this.setHardness(10F)
		.setStepSound(Block.soundAnvilFootstep)
		.setBlockName("BinsCraftBlockExperienceGenerator")
		.setCreativeTab(CreativeTabs.tabDecorations);
		GameRegistry.registerBlock(this, "BinsCraftBlockExperienceGenerator");
		LanguageRegistry.addName(this, "Experience Generator");
		MinecraftForge.setBlockHarvestLevel(this, "pickaxe", 3);
	}
	public String getTextureFile () {
		return BinsCraft.resourceBlockTexturePath;
	}
	
	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int i, float f, float g, float t) {		
		if(player.isSneaking()) {
			return false;
		}
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		if(tileEntity == null) {
			return false;
		}

		player.openGui(BinsCraft.instance, 1, world, x, y, z);
		return true;
	}
	
	@Override
	public TileEntity createNewTileEntity(World var1) {
		return new TileEntityExperienceGenerator();
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, int i, int j) {
		dropItems(world, x, y, z);
		super.breakBlock(world, x, y, z, i, j);
	}
	
	public void dropItems(World world, int x, int y, int z) {
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		if(!(tileEntity instanceof IInventory)) return;
		IInventory inventory = (IInventory) tileEntity;
		tileEntity = null;
		for(int i = 0; i < inventory.getSizeInventory(); i++) {
			ItemStack item = inventory.getStackInSlot(i);
			if(item != null && item.stackSize > 0) {
				EntityItem itemEntity = new EntityItem(world, x, y, z, new ItemStack(item.itemID, item.stackSize, item.getItemDamage()));
				if(item.hasTagCompound()) {
					itemEntity.func_92014_d().setTagCompound((NBTTagCompound) item.getTagCompound().copy());
				}
				world.spawnEntityInWorld(itemEntity);
				item.stackSize = 0;
			}
		}
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public int getBlockTextureFromSideAndMetadata(int side, int meta)
	{
		if(side == 0) return blockIndexInTexture + 2;
		if(side == 1) return blockIndexInTexture + 1;
		return blockIndexInTexture;
	}
}
