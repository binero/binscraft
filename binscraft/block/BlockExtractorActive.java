package binscraft.block;

import net.minecraft.world.IBlockAccess;
import binscraft.inventory.TileEntityExtractor;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockExtractorActive extends BlockExtractor {
	public BlockExtractorActive(int id) {
		super(id);
		GameRegistry.registerBlock(this, "BinsCraftBlockExtractorActive");
		setBlockName("BinsCraftBlockExtractorActive");
		LanguageRegistry.addName(this, "Experience Extractor (active)");
		setLightValue(0.875F);
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public int getBlockTextureFromSideAndMetadata(int side, int meta)
	{
		if(side == meta || (meta == 0 && side == 3)) {
			return blockIndexInTexture + 4;
		}
		if(side == 0) return blockIndexInTexture + 3;
		if(side == 1) return blockIndexInTexture + 2;
		return blockIndexInTexture + 1;
	}
}
