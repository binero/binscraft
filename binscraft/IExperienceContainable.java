package binscraft;

import net.minecraft.item.ItemStack;

public interface IExperienceContainable {
	public int takeCharge(ItemStack itemStack);
	public int takeCharge(ItemStack itemStack, int max);
}
