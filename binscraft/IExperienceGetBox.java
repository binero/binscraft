package binscraft;

public interface IExperienceGetBox {
	public int getExperienceLevel();
	public int takeExperience(int ammount);
}
